#include <vwgl/vwgl.hpp>

using namespace vwgl;

class GUI : public vwgl::scene::SceneComponent {
public:
	enum TestShape {
		kShapeSphere = 0,
		kShapeCube,
		kShapePlane
	};

	GUI(scene::Node * test)
		: _scroll(0)
		, _menuId(0)
		, _nmapDepthId(0)
		, _depthString("10")
		, _useSobel(true)
		, _testObject(test)
		, _blur(false)
		, _blurRadius("1")
	{
		setShape(kShapeSphere);
	}

	virtual void drawUI(vwgl::flat::Renderer & renderer) {
		{
			vwgl::gui::Focus::get().list() = { _menuId, _nmapDepthId, _blurRadiusId };

			Viewport vp = State::get()->getViewport();
			gui::ScrollView view(renderer, _scroll, gui::Layout(20, 20, 300, vp.height() - 40, gui::Layout::kAlignTopLeft));
			gui::Layout layout(3, 3, -28, 25);

			std::vector<std::string> options = { "Sphere", "Cube", "Plane" };
			gui::Controls::menuButton(_menuId, "Select Test Shape", layout, options, [&](int selectedOption) {
				setShape(selectedOption);
			});
			gui::Controls::button("Select Heightmap", layout.getNextDown(), [&]() {
				std::cout << "Select heightmap" << std::endl;
				vwgl::app::FileDialog * dlg = app::FileDialog::openFileDialog();
				if (dlg->show()) {
					_texture = vwgl::Loader::get()->loadTexture(dlg->getResultPath());
					this->rebuildNormalMap();
				}
			});

			if (_texture.valid()) {
				gui::Controls::image(_texture.getPtr(), layout.getNextDown().setWidth(200).setHeight(200));
				gui::Controls::label("Depth:", layout.getNextDown().setWidth(-28).setHeight(25));
				gui::Controls::textField(_nmapDepthId, _depthString, layout.getNextDown());
				bool prevSobel = _useSobel;
				gui::Controls::checkBox("Use sobel filter", layout.getNextDown(), _useSobel);
				if (_useSobel != prevSobel) {
					this->rebuildNormalMap();
				}

				bool prevBlur = _blur;
				gui::Controls::checkBox("Blur", layout.getNextDown(), _blur);
				if (prevBlur != _blur) {
					this->rebuildNormalMap();
				}

				if (_blur) {
					gui::Controls::label("Blur radius:", layout.getNextDown());
					gui::Controls::textField(_blurRadiusId, _blurRadius, layout.getNextDown());
				}

				gui::Controls::button("Apply", layout.getNextDown(), [&]() {
					this->rebuildNormalMap();
				});

				if (_nmap.valid()) {
					gui::Controls::image(_nmap.getPtr(), layout.getNextDown().setWidth(200).setHeight(200));
					gui::Controls::button("Save Normal Map", layout.getNextDown().setWidth(-28).setHeight(25), [&]() {
						vwgl::app::FileDialog * dlg = vwgl::app::FileDialog::saveFileDialog();
						dlg->addFilter("jpg");
						dlg->addFilter("png");
						if (dlg->show()) {
							std::string ext = vwgl::System::get()->getExtension(dlg->getResultPath());
							if (ext == "jpg") {
								this->_nmapImage->saveJPG(dlg->getResultPath());
							}
							else if (ext == "png") {
								this->_nmapImage->savePNG(dlg->getResultPath());
							}
						}
					});
				}
			}

			view.endScroll(layout.getNextDown(), _scroll);
		}
	}

protected:
	int _scroll;
	unsigned int _menuId;
	unsigned int _nmapDepthId;
	unsigned int _blurRadiusId;
	float _depth;
	std::string _depthString;
	vwgl::ptr<vwgl::Texture> _texture;
	vwgl::ptr<vwgl::Image> _nmapImage;
	vwgl::ptr<vwgl::Texture> _nmap;
	bool _useSobel;
	bool _blur;
	std::string _blurRadius;
	ptr<scene::Node> _testObject;

	void rebuildNormalMap() {
		if (_texture.valid()) {
			std::string srcFile = this->_texture->getFileName();
			_depth = static_cast<float>(atof(_depthString.c_str()));
			this->_nmapImage = vwgl::Image::buildNormalMap(vwgl::Loader::get()->loadImage(srcFile), this->_depth, _useSobel);
			int radius = atoi(_blurRadius.c_str());
			if (_blur && radius>0) {
				this->_nmapImage = Image::gaussianBlur(this->_nmapImage.getPtr(), radius);
			}
			this->_nmap = vwgl::TextureManager::get()->loadTexture(this->_nmapImage.getPtr(), true);
			vwgl::scene::Drawable * drw = this->_testObject->getComponent<vwgl::scene::Drawable>();
			if (drw) {
				drw->getGenericMaterial(0)->setNormalMap(this->_nmap.getPtr());
			}
		}
	}

	void setShape(int shape) {
		vwgl::ptr<vwgl::scene::Drawable> drw;
		vwgl::ptr<vwgl::scene::Transform> trx = new vwgl::scene::Transform();
		switch (shape) {
		case kShapeSphere:
			drw = vwgl::scene::PrimitiveFactory::sphere(1.0f, 70);
			break;
		case kShapePlane:
			drw = vwgl::scene::PrimitiveFactory::plane(3.0f, 3.0f);
			drw->getMaterial(0)->setCullFace(false);
			trx->getTransform().getMatrix().rotate(vwgl::Math::degreesToRadians(90.0f), 1.0f, 0.0f, 0.0f);
			break;
		case kShapeCube:
			drw = vwgl::scene::PrimitiveFactory::cube(2.0f);
			break;
		}

		if (drw.valid()) {
			_testObject->addComponent(drw.getPtr());
			_testObject->addComponent(trx.getPtr());
			drw->getGenericMaterial(0)->setShininess(10.0f);

			if (_nmap.valid()) {
				drw->getGenericMaterial(0)->setNormalMap(_nmap.getPtr());
			}
		}
	}
};

class MyWindowController : public vwgl::app::DefaultWindowController {
public:
	MyWindowController() :vwgl::app::DefaultWindowController(vwgl::app::DefaultWindowController::kQualityHigh) {}

protected:

	virtual void configureRenderer(vwgl::scene::Renderer * renderer) {
		vwgl::SSAOMaterial * ssao = renderer->getFilter<vwgl::SSAOMaterial>();
		if (ssao) {
			ssao->setEnabled(false);
		}
	}

	virtual void initScene(vwgl::scene::Node * sceneRoot) {

		vwgl::scene::Node * cubeNode = new vwgl::scene::Node("Cube");
		cubeNode->addComponent(vwgl::scene::PrimitiveFactory::cube());
		sceneRoot->addChild(cubeNode);
		sceneRoot->addComponent(new GUI(cubeNode));

		vwgl::GenericMaterial * mat = cubeNode->getComponent<scene::Drawable>()->getGenericMaterial(0);
		if (mat) {
			mat->setShininess(10.0f);
		}

		vwgl::scene::Node * cameraNode = new vwgl::scene::Node("Camera");
		cameraNode->addComponent(new vwgl::scene::Camera());
		cameraNode->addComponent(new vwgl::scene::Transform());
		cameraNode->addComponent(new vwgl::manipulation::TargetInputController());
		sceneRoot->addChild(cameraNode);

		scene::Light * l1 = new scene::Light();
		l1->setType(ILight::kTypePoint);
		l1->setAmbient(0.2f);
		l1->setSpecular(Color(0.9f, 0.91f, 1.0f, 1.0f));
		scene::Node * l1n = new scene::Node("l1n");
		l1n->addComponent(l1);
		l1n->addComponent(new scene::Transform(Matrix4::makeTranslation(3.0f, 0.5f, 2.0f)));
		sceneRoot->addChild(l1n);

		scene::Light * l2 = new scene::Light();
		l2->setType(ILight::kTypePoint);
		l2->setAmbient(0.0f);
		l2->setSpecular(Color(1.0f, 0.91f, 1.0f, 1.0f));
		scene::Node * l2n = new scene::Node("l2n");
		l2n->addComponent(l2);
		l2n->addComponent(new scene::Transform(Matrix4::makeTranslation(-3.0f, -0.5f, -2.0f)));
		sceneRoot->addChild(l1n);
	}


};


int commonMain() {
	vwgl::Graphics::get()->useApi(vwgl::Graphics::kApiOpenGLAdvanced);

	vwgl::app::Window * window = vwgl::app::Window::newWindowInstance();
	window->setWindowController(new MyWindowController());
	window->setSize(1024, 800);
	window->setPosition(100, 100);
	window->setTitle("Normal Map Generator");
	window->create();

	vwgl::app::MainLoop::get()->setWindow(window);
	return vwgl::app::MainLoop::get()->run();
}

#if VWGL_WINDOWS==1

#include <Windows.h>

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	return commonMain();
}

#else

int main(int argc, char ** argv) {
	return commonMain();
}

#endif